from math import factorial, log2, comb, pi

print("a: ", factorial(30) % 59)
print("b: ", 2**100 % 7)
print("c: ", int('9'*99)//25)
print("d: ", (33**33).bit_length())
print("e: ", len(str((33**33))))
print("f min: ", min(abs(-10), abs(5), abs(20), abs(-35)))
print("f max: ", max(abs(-10), abs(5), abs(20), abs(-35)))
print("g: ", pi*(3**2))
print("h: ", comb(20, 10))
print("i: ", log2(3_100_000_000))
