def phrases():
    with open("day04.txt", "rt") as infile:
        for line in infile:
            yield line


def is_valid(passphrase):
    words = passphrase.split()
    return len(words) == len(set(words))


def is_valid2(passphrase):
    words = ["".join(sorted(w)) for w in passphrase.split()]
    return len(words) == len(set(words))


nvalid = 0
for phrase in phrases():
    nvalid += is_valid(phrase)
print(nvalid)


nvalid = 0
for phrase in phrases():
    nvalid += is_valid2(phrase)
print(nvalid)


def test_example_1_1():
    assert is_valid("aa bb cc dd ee")


def test_example_1_2():
    assert not is_valid("aa bb cc dd aa")


def test_example_1_3():
    assert is_valid("aa bb cc dd aaa")


def test_example_2_1():
    assert is_valid2("abcde fghij")


def test_example_2_2():
    assert not is_valid2("abcde xyz ecdab")


def test_example_2_3():
    assert is_valid2("a ab abc abd abf abj")


def test_example_2_4():
    assert is_valid2("iiii oiii ooii oooi oooo")


def test_example_2_5():
    assert not is_valid2("oiii ioii iioi iiio")