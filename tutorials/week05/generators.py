from math import factorial


def factorials():
    fact = 1
    n = 0
    while True:
        yield fact
        n += 1
        fact *= n


def triangular_numbers():
    last = 0
    n = 0
    while True:
        yield last
        n += 1
        last += n


def test_factorials():
    fact_gen = factorials()
    assert [next(fact_gen) for _ in range(10)] == [factorial(i) for i in range(10)]


def test_triangular_numbers():
    t_num_gen = triangular_numbers()
    assert [next(t_num_gen) for _ in range(10)] == [0, 1, 3, 6, 10, 15, 21, 28, 36, 45]
