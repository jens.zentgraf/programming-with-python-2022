def read_cipher(filename):
    cipher = dict()
    with open(filename, 'rt') as f:
        for line in f:
            letter, code = line.split("\t")
            assert letter not in cipher
            cipher[letter] = int(code, 2)

    assert len(cipher) == 26
    assert len(set(cipher.values())) == 26
    return cipher


def encode_baconian(cipher, string):
    b = bytearray()
    for s in string:
        if s != ' ':
            b.append(cipher[s.upper()])
    return b


def decode_baconian(cipher, b):
    decoding = {value: key for (key, value) in cipher.items()}
    message = []
    for letter in b:
        message.append(decoding[letter])
    return ''.join(message)

    
cipher = read_cipher('cipher.txt')
with open('test.txt', 'wb') as f:
    f.write(encode_baconian(cipher, 'baconian Code'))

with open('test.txt', 'rb') as f:
    print(decode_baconian(cipher, f.read()))


def test_coding_decoding():
    assert decode_baconian(cipher, encode_baconian(cipher, "Test")) == "TEST"
    assert decode_baconian(cipher, encode_baconian(cipher, "baconian Code")) == "BACONIANCODE"
    assert decode_baconian(cipher, encode_baconian(cipher, "secret message")) == "SECRETMESSAGE"
