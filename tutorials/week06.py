from math import sqrt

def modulo7(x):
    return x % 7

def sorted_mod_7(inlist):
    inlist.sort(reverse=True, key=lambda x: x % 7)
    return inlist


def test_sorted():
    assert sorted_mod_7([5, 48, 98]) == [48, 5, 98]

def test_lambda():
    assert [48, 5, 98] == (lambda x: sorted(x, reverse=True, key=lambda x: x % 7))([5, 48, 98])


with open("numbers.txt", "rt") as infile:
    numbers = infile.read().split()

list_c = [int(x) for x in numbers]
list_m = list(map(int, numbers))
assert list_m == list_c

square_l = [(int(sqrt(int(x))) + 1) ** 2 for x in numbers]
square_m = list(map(lambda x: (int(sqrt(int(x))) + 1) ** 2, numbers))
assert square_l == square_m