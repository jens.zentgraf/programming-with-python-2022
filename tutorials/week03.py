from math import prod
from collections import Counter, defaultdict

# Warm up: Counting digits
# `Long` solution using dicts
def duplicate_count(text):
    counts = dict()
    for c in text:
        c = c.upper()
        if c not in counts:
            counts[c] = 1
        else:
            counts[c] += 1

    repeats = 0
    for value in counts.values():
        if value > 1:
            repeats += 1
    return repeats


# `Short` solution
def duplicate_count2(text):
    return len([c for c in set(text.lower()) if text.lower().count(c) > 1])


# Warm up: Multiplicative persistence
def persistence(n):
    steps = 0
    while n > 9:
        n = prod([int(i) for i in str(n)])
        steps += 1
    return steps


print("Duplicate counts 'abcde': %x" % duplicate_count2("abcde"))
print("Duplicate counts 'abBcdea': %x" % duplicate_count2("abBcdea"))
print("Duplicate counts 'counting': %x" % duplicate_count2("counting"))

print("Persistence 39: %x " % persistence(39))
print("Persistence 999: %x " % persistence(999))
print("Persistence 8: %x " % persistence(8))

raw_data = """>ENA|AAF26411|AAF26411.1 TGEV spike protein expression construct truncated spike protein
ATGGCTTTCTTGAAGAGTTTCCCATTCTACGCTTTCTTGTGCTTCGGACAATACTTCGTG
GCTGTGACTCACGCAGACAACTTCCCATGCTCTAAGTTGACCAACAGGACCATCGGTAAT
CAATGGAACTTGATCGAGACCTTCTTGTTGAACTACTCATCTAGGTTGCCACCAAACTCT
GACGACGTGTTGGGTGACTACTTCCCAACTGTGCAACCTTGGTTCAACTGCATCAGGAAC
AACTCTAACGACTTGTACGTGACTTTGGAGAACTTGAAGGCTCTCTACTGGGACTACGCT
ACTGAGAACATCACCTGGAACCACAGGCAAAGGTTGAACGTGGTGGTGAACGGATACCCA
TACAGTATCACAGTGACAACAACCCGCAACTTCAACTCTGCTGAGGGTGCTATTATCTGC
ATTTGCAAGGGAAGTCCACCAACTACCACCACCGAGTCTAGTTTGACTTGCAACTGGGGA
AGTGAGTGCAGGTTGAACCACAAGTTCCCTATCTGTCCATCTAACTCAGAGGCAAACTGC
GGAAACATGCTGTACGGCTTGCAATGGTTCGCAGACGAGGTGGTGGCTTACTTGCATGGA
GCTAGTTACCGGATTAGTTTCGAGAACCAATGGTCTGGCACTGTGACATTCGGTGACATG
CGGGCCACAACATTGGAGGTGGCTGGCACGTTGGTGGACTTGTGGTGGTTCAACCCAGTG
TACGATGTCAGTTACTACAGGGTGAACAACAAGAACGGGACTACCGTGGTGAGCAACTGC
ACTGACCAATGCGCTAGTTACGTGGCTAACGTGTTCACTACACAGCCAGGAGGATTCATC
CCATCAGACTTTAGTTTCAACAACTGGTTCCTCTTGACTAACAGCAGCACTTTGGTGAGT
GGTAAGTTGGTGACCAAGCAGCCGTTGCTCGTTAACTGCTTGTGGCCAGTCCCAAGCTTC
GAGGAGGCAGCTTCTACATTCTGCTTCGAGGGAGCTGGCTTCGACCAATGCAATGGAGCT
GTGCTCAACAATACTGTGGACGTGATTAGGTTCAACCTCAACTTCACTACAAACGTGCAA
TCAGGGAAGGGTGCCACAGTGTTCTCATTGAACACAACCGGTGGAGTCACTCTCGAGATT
TCATGCTACACAGTGAGTGACTCGAGCTTCTTCAGTTACGGAGAGATTCCGTTCGGCGTG
ACTGACGGACCACGGTACTGCTACGTGCACTACAACGGCACAGCTCTCAAGTACCTCGGA
ACACTCCCACCTAGTGTGAAGGAGATTGCTATCAGTAAGTGGGGCCACTTCTACATTAAC
GGTTACAACTTCTTCAGCACATTCCCAATTGACTGCATCTCATTCAACTTGACCACTGGT
GACAGTGACGTGTTCTGGACAATCGCTTACACAAGCTACACTGAGGCACTCGTGCAAGTT
GAGAACACAGCTATTACAAAGGTGACGTACTGCAACAGTCACGTTAACAACATTAAGTGC
TCTCAAATTACTGCTAACTTGAACAACGGATTCTACCCTGTTTCTTCAAGTGAGGTTGGA
CTCGTGAACAAGAGTGTTGTGCTCCTCCCAAGCTTCTACACACACACCATTGTGAACATC
ACTATTGGGCTCGGAATGAAGCGTAGTGGGTACGGGCAACCAATCGCCTCAACATTGAGT
AACATCACATTGCCAATGCAGGACCACAACACCGATGTGTACTGCATTCGGTCTGACCAA
TTCTCAGTTTACGTGCATTCTACTTGCAAGAGTGCTTTGTGGGACAATATTTTCAAGCGA
AACTGCACGGACCACCACCATCACCATCACTAA
"""


def clean_up(raw):
    raw = raw.splitlines()
    if raw[0].startswith(">"):
        seq = "".join(raw[1:])
    else:
        seq = "".join(raw)
    return seq


seq = clean_up(raw_data)
counts = Counter(seq)
print(f"Count the numer of occurences of each nucleotide: {counts}")
print(f"What is the GC content if the sequence? {(counts['C'] + counts['G'])/len(seq)}")


def list_kmers(seq, k):
    kmers = []

    for i in range(len(seq) - k + 1):
        kmers.append(seq[i:i + k])

    return kmers


def test_list_kmers_k_greater_s():
    kmers = list_kmers("AAA", 5)
    assert isinstance(kmers, list)
    assert len(kmers) == 0


def test_list_kmers():
    kmers = list_kmers("ACGTGTCGATC", 7)
    assert len(kmers) == 5
    assert kmers == ["ACGTGTC", "CGTGTCG", "GTGTCGA", "TGTCGAT", "GTCGATC"]


def number_of_unique(seq, k):
    kmers = defaultdict(int)

    for i in range(len(seq) - k + 1):
        kmers[seq[i:i + k]] += 1

    return sum(1 for count in kmers.values() if count == 1)


def test_number_of_unique_example():
    assert number_of_unique("AAAA", 2) == 0


def test_number_of_unique():
    number_of_unique("ACGTGTCGATC", 7) == 5


ENCODING = {
    "A": 0,
    "C": 1,
    "G": 2,
    "T": 3,
}


def kmer_code(kmer):
    code = 0

    for c in kmer:
        code = code << 2 | ENCODING[c]

    return code


def test_kmer_code_example():
    assert kmer_code("ACGT") == 27


def canonical_code(kmer):
    code = kmer_code(kmer)
    rev_code = 0

    for i in range(len(kmer)):
        rev_code = 3 - ((code >> i) & 3)

    return max(code, rev_code)


def test_canoncical_code_example():
    canonical_code("AAAC") == 191
    canonical_code("GGGT") == 191
