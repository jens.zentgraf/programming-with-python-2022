from collections import Counter


def read_first_fasta(filename):
    genome = []
    with open(filename, 'rt') as fasta_file:
        for line in fasta_file:
            if line.startswith('>'):
                if genome:
                    break
                continue
            genome.append(line.strip())
    return ''.join(genome)


def read_genetic_code(filename):
    genetic_code = dict()
    with open(filename, 'rt') as f:
        for line in f:
            aa, codonstring = line.split(":")
            codons = codonstring.strip().split()
            for codon in codons:
                assert codon not in genetic_code, f"codon {codon} already present: {genetic_code[codon]}"
                genetic_code[codon] = aa
    assert len(genetic_code) == 64
    assert len(set(genetic_code.values())) == 21
    return genetic_code


def convert_to_protein(sequence, code):
    n = len(sequence)
    index = 0
    while True:
        index = sequence.find('ATG', index)
        if index == -1:
            break
        protein = []
        codon = sequence[index:index+3]
        while code[codon] != 'STOP':
            protein.append(code[codon])
            index += 3
            if index > n-3:
                return
            codon = sequence[index:index+3]
        yield ''.join(protein)


fasta_file = 'sequence.fasta'
sequence = read_first_fasta(fasta_file)
print("Sequence statistics:", Counter(sequence))

code_file = 'geneticCode.txt'
code = read_genetic_code(code_file)

proteins = convert_to_protein(sequence, code)
with open('proteins.txt', 'wt') as fout:
    for p in proteins:
        print(p, file=fout)
