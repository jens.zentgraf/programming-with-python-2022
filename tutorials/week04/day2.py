def checksum(numbers):
    return max(numbers) - min(numbers)


def divisible(checksums):
    for i, n1 in enumerate(checksums[:-1]):
        for n2 in checksums[i + 1:]:
            if n1 % n2 == 0:
                return n1 // n2
            if n2 % n1 == 0:
                return n2 // n1


def main():
    with open("2.txt", "rt") as infile:
        # print(f"part 1: {sum(checksum(list(map(int, line.split()))) for line in infile)}")
        checksums = []
        for line in infile:
            checksums.append(checksum([int(i) for i in line.split()]))
        print(f"part 1: {sum(checksums)}")

    with open("2.txt", "rt") as infile:
        # print(f"part 2: {sum(devisible(list(map(int, line.split()))) for line in infile)}")
        numbers = []
        for line in infile:
            numbers.append(divisible([int(i) for i in line.split()]))
        print(f"part 1: {sum(numbers)}")


if __name__ == "__main__":
    main()


def test_p1_1():
    assert checksum([5, 1, 9, 5]) == 8


def test_p1_2():
    assert checksum([7, 5, 3]) == 4


def test_p1_3():
    assert checksum([2, 4, 6, 8]) == 6


def test_p2_1():
    assert divisible([5, 9, 2, 8]) == 4


def test_p2_2():
    assert divisible([9, 4, 7, 3]) == 3


def test_p2_3():
    assert divisible([3, 8, 6, 5]) == 2
