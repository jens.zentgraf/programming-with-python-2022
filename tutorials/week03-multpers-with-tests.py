from math import prod  # like sum, but a product


def multiplicative_persistence(n):
    steps = 0  # count the number of steps
    while len(s := str(n)) > 1:
        n = prod([int(digit) for digit in s])
        # advanced: n = prod(map(int, s))
        steps += 1
    return steps


def largest_multiplicative_persistence(start, end):
    return max((multiplicative_persistence(n), n) for n in range(start, end))


def test_8():
    assert multiplicative_persistence(8) == 0


def test_999():
    assert multiplicative_persistence(999) == 4


def test_39():
    assert multiplicative_persistence(39) == 3


if __name__ == "__main__":
    maxmp, maxn = largest_multiplicative_persistence(0, 10000)
    print(f"Largest multiplicative persistence in 0 .. 9999: {maxmp} for {maxn}")
    n = input("Of which number do you want to know the multiplicative persistence? ")
    print(f"The multiplicative persistence of {n} is {multiplicative_persistence(int(n))}.")
